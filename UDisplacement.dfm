object FDisplacement: TFDisplacement
  Left = 1617
  Top = 526
  Width = 192
  Height = 236
  BorderIcons = [biSystemMenu]
  Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103' '#1091#1079#1083#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 3
    Top = 8
    Width = 170
    Height = 150
    ColCount = 2
    DefaultColWidth = 80
    FixedCols = 0
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object bOk: TBitBtn
    Left = 42
    Top = 165
    Width = 90
    Height = 25
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
end
