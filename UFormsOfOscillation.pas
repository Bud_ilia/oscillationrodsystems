unit UFormsOfOscillation;
{-----------------------------------------------------------------------------}
                                 interface
{-----------------------------------------------------------------------------}
USES ULib_Common;
{-----------------------------------------------------------------------------}
PROCEDURE DeterminationOscillationShape(Omega:extended; NumUsel,NumSv: integer);
PROCEDURE MovingNodesGlobal(Omega: extended; NumUsel,NumSv: integer; var DLT: V_H);
PROCEDURE LocalMovesCrossSection(Omega: extended;  m: integer);
PROCEDURE GlobalMovesCrossSection(m: integer);
{-----------------------------------------------------------------------------}
                              implementation
USES UGeneral;
{-----------------------------------------------------------------------------}
PROCEDURE DeterminationOscillationShape(Omega:extended; NumUsel,NumSv: integer);
var i,k1,k2,m: integer;
    VDG,VDL: V_2NS;
    MT: M_2NS;
BEGIN
  {��� �������� ������� ����������� ��������� Omega*
   ������� ��������������� ����� ����������� ���������, �������� ��������� ��������:
   1. ��������������� ���������� ������� ������� MGmod, ������� ���� �� ����������� ������ 1
      � ������� ��������������� ������ ������� MGmod, ����� ������������� ��������;
   2. ��������� ������� ���������� ������ ��������, � ������� ������ ���� ��������� ����� 1;
   3. ������ ���������� ����� ������� ������� �������� �������������� ���������,
      ��������� ��� ����� ����������� ����� � ���������� �����������.
   ��� ��� �������� ��������� ��������� MovingNodesGlobal}

  MovingNodesGlobal(Omega,NumUsel,NumSv, DLT);
  {����������� ����� ����������� � ���������� ����������� ����������}
  
  {��������� ������� ����������� ����� ���������� ��������� � ��������� �����������}
  for m:=1 to NE do
    begin
      k1:=EL[m].k1;     k2:=EL[m].k2;
      for i:=1 to NS do VDG[i]  :=US[k1].D[i];
      for i:=1 to NS do VDG[i+1]:=US[k2].D[i];
      {��������� ������� �������� ��� ����������� ��������}
       FORM_MT(m,MT);
       {��������� ������ ����������� ����� ����������� �������� m � ��������� �����������}
       U_MV(2*NS,MT,VDG, VDL);
       for i:=1 to 2*NS do EL[m].DL[i]:=VDL[i];
    end;
  {����������� ����� ����������� � ��������� ����������� ����������}

  {���������� ����������� ���������� ������� ���������� ��������� � ��������� �����������}
  for m:=1 to NE do LocalMovesCrossSection(Omega,m);

  {���������� ����������� ���������� ������� ���������� ��������� � ���������� �����������}
  for m:=1 to NE do GlobalMovesCrossSection(m);
  {����������� ���� ���������� ������� ���� ���������� ��������� � ���������� ����������� ����������}

END;  {DeterminationOscillationShape}
{-----------------------------------------------------------------------------}
PROCEDURE MovingNodesGlobal(Omega: extended; NumUsel,NumSv: integer; var DLT: V_H);
{���������� ����������� ����� � ���������� ����������� � ��������� �� ���������� =1
 ��� ��������� �������� ������� ����������� ���������}
{NumUsel-����� ����, NumSv-����� ��������� ����� � ���� ���� (� ������ ������ NumSv=1);  DLT - ������ ����������� �����}
var i,j,k: integer;
    A    : M_H;                                              //M_H = array [1..Nmax, 1..Nmax]
    KG   : M_2NS;                                            //M_2NS = array [1..2*NS, 1..2*NS]
    B    : V_H;                                              //V_H = array [1..Nmax]
BEGIN
  {��������� ���������� ������� ��������� ���������� ���������}
  for i:=1 to NE do
    begin FORM_KG(Omega,i,KG);   EL[i].KG:=KG; end;
  {��������� ���������� ��������� ���������� ���������}
  for i:=1 to NE do
    Form_Kij(i,EL[i].KG, EL[i].K11,EL[i].K12,EL[i].K21,EL[i].K22);

  {��������� ���������� ������� ��������� �������  MG}
  FORM_MGlobal(MG);

  {������������ ������� MG    (MG => MGmod)}
  Boundary_Data(MG, MGmod);

  A:=MGmod;
  {������������ ������� A=MGmod}
  k:=NS*(NumUsel-1)+NumSv;
  for j:=1 to Nmax do A[k,j]:=0;
  A[k,k]:=1;
  {��������� ������� ������ � ����� �����������, ������ 1}
  for j:=1 to Nmax do B[j]:=0;
  B[k]:=1;
  GAUSS(NU*NS,A,B, DLT);
  {��������� ����������� ����� ����������� � ������� US}
  for i:=1 to NU do
    for j:=1 to NS do  US[i].D[j]:=DLT[(i-1)*NS+j];
END; {MovingNodesGlobal}
{-----------------------------------------------------------------------------}
PROCEDURE LocalMovesCrossSection(Omega: extended;  m: integer);
{����������� ����������� ���������� ������� ����������� �������� m � ��������� ����������� - ����� ���������}
var Y0,Y : MMY;               //MMY = array [1..NV] of Vector;    Vector = array [1..PD] of extended;  (U, u)  (Y[1,1]=U1, Y[1,2]=u1;  Y[2,1]=U2, Y[2,2]=u2;)
    (*U    : MMA;
    CW,BW: VB; *)             //VB = array [1..NV-1]  of extended;  � ������ ������ VB = array [1..1] of extended;
    X0,C1,C2 : extended;
    i,j,JF,KW: integer;
BEGIN
  {��������� ��������� ������� Y0}
  Y0[1,1]:=1; {U1}    Y0[1,2]:=0; {u1}
  Y0[2,1]:=0; {U2}    Y0[2,2]:=1; {u2}
 { Y0:=Y00;
  Y0[2,2]:=EL[m].DL[1]; }
  JF:=0;                                 //JF - ������ ������� �����:  (0..MS)
  Y:=Y0;
  Z[JF]:=Y;

  {��������� ��������� ������� ������� ���������������� ���������,
   � ���������� �������� ������� ������� Y(Xk) � ����� ����������� ��������}
      for KW:=1 to MS do
        begin
          X0:=(KW-1)*DX[m];
          RKT(Omega,m,X0,DX[m],NP,Y0,RightMember_0,Y);
          INC(JF);
          Z[JF]:=Y;
          Y0:=Y;
        end;

(*  {��������� ������� �������� �������������� ��������� ��� ����������� ���������� ��������������.
   � ������ ������ - ��� ������ ���� ���������� �������������� � �������������� ������ ���� ���������}
  for i:=1 to NV-1 do
    begin
      for j:=1 to NV-1 do U[i,j]:=Y[j,i+1];        U[1,1]:=Y[1,2];
      BW[i]:=-Y[NV,i+1] +EL[m].DL[i+1];            BW[1]:=-Y[2,2] +EL[m].DL[2];
    end;
  {������ ������� �������� �������������� ���������, ���������� ���������� ��������������}
  CW[1]:=BW[1]/U[1,1];                             CW[1]:=BW[1]/Y[1,2];

  //���������� ��������� ��� � ������� ������
  for i:=0 to MS do
    for j:=1 to PD do YS[i,j]:=CW[1]*Z[i,1,j]+Z[i,2,j]; *)

  {���������� ���������� ��������������}
  C1:=(EL[m].DL[2]-EL[m].DL[1]*Y[2,2])/Y[1,2];
  C2:=EL[m].DL[1];

  //���������� ��������� ��� � ������� ������
  for i:=0 to MS do
    for j:=1 to PD do YS[i,j]:=C1*Z[i,1,j]+C2*Z[i,2,j];

  {���������� ������ �����������  UL  ���������� ������� ����������� �������� m � ��������� �����������}
  for i:=0 to MS do UL[m,i]:=YS[i,2];
END;  {LocalMovesCrossSection}
{-----------------------------------------------------------------------------}
PROCEDURE GlobalMovesCrossSection(m: integer);
{����������� ����������� ���������� ������� ����������� �������� m � ���������� �����������}
var i: integer;
BEGIN
  for m:=1 to NE do
    for i:=0 to MS do UG[m,i]:=UL[m,i]*EL[m].cf;
END;  {GlobalMovesCrossSection}
{-----------------------------------------------------------------------------}


end.
