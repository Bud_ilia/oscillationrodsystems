program OscillationRodSystems;
//����������� ��������� ���������� ������. 
//������� �������� �� ������ �������������� ������� ���������������� ���������
//������� ��������� ����������.
//����������� ����������� ������� ��������� � �������� ��������� �� ���������
//������������ ��������������� ����� ���������

uses
  Forms,
  UTitul in 'UTitul.pas' {FTitul},
  UHead in 'UHead.pas' {FHead},
  UGeneral in 'UGeneral.pas',
  ULib_Common in 'ULib_Common.pas',
  UInitData in 'UInitData.pas' {FInitData},
  UDisplacement in 'UDisplacement.pas' {FDisplacement},
  UFormsOfOscillation in 'UFormsOfOscillation.pas',
  UDrawing in 'UDrawing.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFTitul, FTitul);
  Application.CreateForm(TFHead, FHead);
  Application.CreateForm(TFInitData, FInitData);
  Application.CreateForm(TFDisplacement, FDisplacement);
  Application.Run;
end.
