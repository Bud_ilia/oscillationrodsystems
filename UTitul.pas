unit UTitul;
{-----------------------------------------------------------------------------}
                                    interface
{-----------------------------------------------------------------------------}
uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls;

type
  TFTitul = class(TForm)
    Timer1: TTimer;
    Label1: TLabel;
    Label2: TLabel;
    procedure OKBtnClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var FTitul: TFTitul;
{-----------------------------------------------------------------------------}
                           implementation
uses UHead;
                              {$R *.dfm}
{-----------------------------------------------------------------------------}
procedure TFTitul.OKBtnClick(Sender: TObject);
begin
  FTitul.Visible:=False;
  START;
  INIT_DATA;
  FHead.Show;
end;

procedure TFTitul.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  FTitul.Visible:=False;
  START;
  INIT_DATA;
  FHead.Show;

end;

end.
