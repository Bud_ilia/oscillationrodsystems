unit UInitData;
{-----------------------------------------------------------------------------}
                                 interface
{-----------------------------------------------------------------------------}
uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, Grids, StdCtrls, Buttons, ExtCtrls, ComObj;
{-----------------------------------------------------------------------------}
type
  TFInitData = class(TForm)
    StringGrid1: TStringGrid;
    bOK: TBitBtn;
    bCancel: TBitBtn;
    Label1: TLabel;
    StringGrid2: TStringGrid;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Label3: TLabel;
    ComboBox2: TComboBox;
    Label4: TLabel;
    procedure bOKClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  
PROCEDURE Cleaning_Table(StringGrid: TStringGrid);
PROCEDURE FormTablInitData_Excel;
PROCEDURE InputData;
PROCEDURE Form_NullLink;
PROCEDURE GEOMETRICS;

var FInitData: TFInitData;
    NumU,NumE: integer;
{-----------------------------------------------------------------------------}
                              implementation
USES UGeneral, UHead, ULib_Common, UDrawing;
                                {$R *.dfm}
{-----------------------------------------------------------------------------}
PROCEDURE Cleaning_Table(StringGrid: TStringGrid);
var iRow,iCol: integer;
BEGIN
  for iRow:=0 to StringGrid.RowCount do
    for iCol:=0 to StringGrid.ColCount do  StringGrid.Cells[iRow,iCol]:='';
END;  {Cleaning_Table}
{-----------------------------------------------------------------------------}
PROCEDURE FormTablInitData_Excel;
var IRow,k: integer;
    ExcelApplicationl,ExcelWorkBookl,ExcelWorksheetl: OleVariant;
BEGIN
  ExcelApplicationl:=CreateOleObject('Excel.Application');
  ExcelWorkBookl:=ExcelApplicationl.WorkBooks.Add(FHead.OpenDialog1.FileName);

  ExcelWorksheetl:=ExcelWorkbookl.Worksheets[1];
  ExcelWorksheetl.Activate;

  NumU:=StrToInt(ExcelWorkSheetl.Cells[2,1]);
  NumE:=StrToInt(ExcelWorkSheetl.Cells[2,2]);
  FInitData.ComboBox1.Text:=IntToStr(NumU);
  FInitData.ComboBox2.Text:=IntToStr(NumE);

  With FInitData.StringGrid1 do
    begin
      RowCount:=NumU+1;
      Cells[0,0] :='    � ����';
      Cells[1,0] :='    x, ��';
      Cells[2,0] :='    y, ��';
      Cells[3,0] :='������� �����';
      Cells[4,0] :='������� �����';
      Cells[5,0] :='   G1, �/��';
      Cells[6,0] :='   G2, �/��';
      for IRow:=1 to NumU do
        begin
          k:=iRow+4;
          Cells[0,iRow]:=ExcelWorkSheetl.Cells[k,1];
          Cells[1,iRow]:=ExcelWorkSheetl.Cells[k,2];
          Cells[2,iRow]:=ExcelWorkSheetl.Cells[k,3];
          Cells[3,iRow]:=ExcelWorkSheetl.Cells[k,4];
          Cells[4,iRow]:=ExcelWorkSheetl.Cells[k,5];
          Cells[5,iRow]:=ExcelWorkSheetl.Cells[k,6];
          Cells[6,iRow]:=ExcelWorkSheetl.Cells[k,7];
        end;
    end;

  With FInitData.StringGrid2 do
    begin
      RowCount:=NumE+1;
      Cells[0,0] :=' � ��������';
      Cells[1,0] :='      i';
      Cells[2,0] :='      j';
      Cells[3,0] :='     E, ���';
      Cells[4,0] :='     F, ��2';
      Cells[5,0] :='    T, ����';
      Cells[6,0] :='  ���������, ��/�3';
      for IRow:=1 to NumE do
        begin
          k:=iRow +4+NumU+2;
          Cells[0,iRow]:=ExcelWorkSheetl.Cells[k,1];
          Cells[1,iRow]:=ExcelWorkSheetl.Cells[k,2];
          Cells[2,iRow]:=ExcelWorkSheetl.Cells[k,3];
          Cells[3,iRow]:=ExcelWorkSheetl.Cells[k,4];
          Cells[4,iRow]:=ExcelWorkSheetl.Cells[k,5];
          Cells[5,iRow]:=ExcelWorkSheetl.Cells[k,6];
          Cells[6,iRow]:=ExcelWorkSheetl.Cells[k,7];
        end;
    end;
  ExcelApplicationl.Workbooks.Close;
  ExcelApplicationl.Quit;
  ExcelApplicationl:=UnAssigned;
  ExcelWorksheetl:=UnAssigned;
END;  {FormTablInitData_Excel}
{-----------------------------------------------------------------------------}
PROCEDURE InputData;
var i,k: integer;
BEGIN
  INIT_DATA;
  NU:=NumU;
  NE:=NumE;
  With FInitData.StringGrid1 do
    begin
      for i:=1 to NU do
        begin
          k:=i;
          US[i].x :=StrToFloat(Cells[1,k]);
          US[i].y :=StrToFloat(Cells[2,k]);
          US[i].S1:=StrToInt(Cells[3,k]);
          US[i].S2:=StrToInt(Cells[4,k]);
          US[i].G1:=StrToFloat(Cells[5,k]);
          US[i].G2:=StrToFloat(Cells[6,k]);
          if US[i].S1=1 then US[i].G1:=0;
          if US[i].S2=1 then US[i].G2:=0;
        end;
    end;
  With FInitData.StringGrid2 do
    begin
      for i:=1 to NE do
        begin
          k:=i;
          EL[i].k1:=StrToInt(Cells[1,k]);
          EL[i].k2:=StrToInt(Cells[2,k]);
          EL[i].E:=StrToFloat(Cells[3,k]);
          EL[i].F:=StrToFloat(Cells[4,k]);
          EL[i].T:=StrToFloat(Cells[5,k]);
          EL[i].Dens:=StrToFloat(Cells[6,k])*1E-12;          //1 ��/�^3 = 1E-12 �*�^2/��^4
        end;
    end;
END;  {InputData}
{-----------------------------------------------------------------------------}
PROCEDURE GEOMETRICS;
{���������� �������������� ���������� ���������� ���������}
var m,k1,k2: integer;
BEGIN
  for m:=1 to NE do
    begin
      k1:=EL[m].k1;   k2:=EL[m].k2;
      EL[m].L:=SQRT(SQR(US[k2].x-US[k1].x) + SQR(US[k2].y-US[k1].y));    //����� ��������
      EL[m].Sf:=(US[k2].y-US[k1].y)/EL[m].L;                             //SinFi  � ���������� ������� ���������
      EL[m].Cf:=(US[k2].x-US[k1].x)/EL[m].L;                             //CosFi
      DX[m]:=EL[m].L/MS;
    end;
END;  {GEOMETRICS}
{-----------------------------------------------------------------------------}
procedure TFInitData.bOKClick(Sender: TObject);
var iRow,j: integer;
    Completion1,Completion2: Boolean;
begin
  NU:=NumU;
  NE:=NumE;

  Completion_Data:=False;
  Completion1:=True;
  Completion2:=True;
  if NU=0 then  Completion1:=False;
  if NE=0 then  Completion2:=False;

  for IRow:=1 to NU do
    for j:=1 to 3 do if FInitData.StringGrid1.Cells[j,iRow]='' then Completion1:=False;
  if not Completion1 then
    MessageDlg('�������� ������ � ������� "����" ������� �� ���������',mtError,[mbOK],0);

  for IRow:=1 to NE do
    for j:=1 to 6 do if FInitData.StringGrid2.Cells[j,iRow]='' then Completion2:=False;
  if not Completion2 then
    MessageDlg('�������� ������ � ������� "��������" ������� �� ���������',mtError,[mbOK],0);

  if Completion1 and Completion2 then Completion_Data:=True else Completion_Data:=False;

  if Completion_Data then
    begin
      InputData;
      GEOMETRICS;
      Draw_Ferma(FHead,NU,NE);
      FHead.Edit1.Text:='';
      FHead.ComboBox3.Text:='';
      FHead.ComboBox4.Text:='';
      FHead.ComboBox3.Items.Clear;
      Form_NullLink;
    end;

  FHead.Panel3.Visible:=True;
  FHead.Panel4.Visible:=False;
  FHead.Panel6.Visible:=False;
  FHead.Invalidate;
end;
{-----------------------------------------------------------------------------}
procedure TFInitData.bCancelClick(Sender: TObject);
begin
  FHead.Invalidate;
  Close;
end;
{-----------------------------------------------------------------------------}
procedure TFInitData.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Draw_Ferma(FHead,NU,NE);
  FHead.Invalidate;
  FHead.Repaint;
end;
{-----------------------------------------------------------------------------}
PROCEDURE Form_NullLink;
var n: integer;
BEGIN
  for n:=1 to NU do
    if (US[n].S1=0) then FHead.ComboBox3.Items.Add(FloatToStr(n));
END;   {Form_NullLink}
{-----------------------------------------------------------------------------}
procedure TFInitData.FormCreate(Sender: TObject);
begin
  StringGrid1.DefaultColWidth:=90;
  StringGrid1.ColWidths[0]:=65;
  StringGrid1.ColWidths[1]:=65;
  StringGrid1.ColWidths[2]:=65;
  
  StringGrid2.DefaultColWidth:=70;
  StringGrid2.ColWidths[0]:=80;
  StringGrid2.ColWidths[6]:=130;
end;
{-----------------------------------------------------------------------------}


end.



