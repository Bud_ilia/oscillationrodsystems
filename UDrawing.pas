unit UDrawing;
{-----------------------------------------------------------------------------}
                                 interface
{-----------------------------------------------------------------------------}
USES SysUtils, Graphics, Forms;
{-----------------------------------------------------------------------------}
PROCEDURE Draw_Ferma(Form: TForm;  NU,NE: integer);
PROCEDURE Draw_Element(Form: TForm;  m: integer);
PROCEDURE Draw_Usel(Form: TForm;  n: integer);
PROCEDURE Draw_Support(Form: TForm;  n: integer);
PROCEDURE ScaleMove(MS: integer; var MasM: extended);
PROCEDURE DrawForm(Form: TForm; MS: integer; Cc: TColor);

{-----------------------------------------------------------------------------}
                              implementation
USES UGeneral, ULib_Common, UHead;
{-----------------------------------------------------------------------------}
var Xk,Yk: integer;
{-----------------------------------------------------------------------------}
PROCEDURE Draw_Ferma(Form: TForm;  NU,NE: integer);
{��������� ����� � ����� �� ����������}
var Xmin,Xmax,Ymin,Ymax,DXmax,DYmax,MASx,MASy: extended;
    i: integer;
BEGIN
  if NU=0 then Exit;
  {���������� �������� ����������� (Xmin,Xmax, Ymin,Ymax � ��)
   � ������� ����������� (MAS, pixel/mm)}
  Xmax:=0;   Xmin:=1E8;    Ymax:=0;   Ymin:=1E8;
  for i:=1 to NU do
    begin
      if US[i].x >Xmax then Xmax:=US[i].x;        //��
      if US[i].x <Xmin then Xmin:=US[i].x;
      if US[i].y >Ymax then Ymax:=US[i].y;
      if US[i].y <Ymin then Ymin:=US[i].y;
    end;
  DXmax:=Xmax-Xmin;          DYmax:=Ymax-Ymin;
  if ABS(DXmax)=0 then MASx:=1E4 else MASx:=DXS/ABS(DXmax);
  if ABS(DYmax)=0 then MASy:=1E4 else MASy:=DYS/ABS(DYmax);
  if MASx<MASy then MAS:=MASx else MAS:=MASy;
  Xk:=Xc-Round(Mas*DXmax/2);   Yk:=Round(Yc+Mas*DYmax/2);   //������ ��������� ��� ���������� �����������

  {��������� ���������� �����������}
  for i:=1 to NE do Draw_Element(Form,i);
  for i:=1 to NU do Draw_Usel(Form,i);
  for i:=1 to NU do Draw_Support(Form, i);
END; {Draw_Ferma}
{-----------------------------------------------------------------------------}
PROCEDURE Draw_Usel(Form: TForm;  n: integer);
{��������� ���� �����������}
var x0,y0: integer;
    x,y: extended;
    Sn: string;
BEGIN
  x:=US[n].x;   y:=US[n].y;
  Sn:=IntToStr(n);
  x0:=Xk+Round(x*Mas);    y0:=Yk-Round(y*Mas);
  With Form.Canvas do
    begin
      Pen.Color:=clGray;  Brush.Color:=clYellow;  Pen.Width:=1;
      Rectangle(x0-L(6),y0-L(6),x0+L(6),y0+L(6));
      Font.Color:=clRed;
      Font.Name:='Arial';
      Font.Style:=[fsBold];    Font.Size:=L(7);
      TextOut(x0-L(3),y0-L(4),Sn);
    end;
END; {Draw_Usel}
{-----------------------------------------------------------------------------}
PROCEDURE Draw_Element(Form: TForm;  m: integer);
{��������� �������� �����������}
var x1,x2,y1,y2,x0,y0,k1,k2: integer;
    Sm: string;
BEGIN
  k1:=EL[m].k1;                  k2:=EL[m].k2;
  x1:=Xk+round(US[k1].x*Mas);    y1:=Yk-round(US[k1].y*Mas);
  x2:=Xk+round(US[k2].x*Mas);    y2:=Yk-round(US[k2].y*Mas);
  x0:=(x1+x2) div 2;             y0:=(y1+y2) div 2;
  Sm:=IntToStr(m);
  With Form.Canvas do
    begin
      Pen.Width:=L(3);
      if El[m].T >0 then Pen.Color:=clFuchSia else Pen.Color:=clYellow;
      MoveTo(x1,y1);  LineTo(x2,y2);
      Font.Color:=clAqua;    Brush.Color:=clBlack;
      Font.Name:='Arial';
      Font.Style:=[fsBold];    Font.Size:=L(8);
      TextOut(x0+L(3),y0+L(4),Sm);
    end;
END; {Draw_Element}
{-----------------------------------------------------------------------------}
PROCEDURE Draw_Support(Form: TForm;  N: integer);
{��������� ������� �����������}
var x0,y0,Typ,k: integer;
    x,y: extended;
BEGIN
  Typ:=0;
  if US[N].S1=1 then Typ:=1;     //������� �������������� ����� � N ����
  if US[N].S2=1 then Typ:=2;     //������� �������������� ����� � N ����
  if Typ=0 then Exit;

  x:=US[N].x;             y:=US[N].y;
  x0:=Xk+round(x*Mas);    y0:=Yk-round(y*Mas);

  k:=1;       //���� N=NU
  if N=1 then k:=-1;

  Case Typ of
    1: With Form.Canvas do                //������� �������������� �����
         begin
           Pen.Width:=L(1);
           Circle(Form, x0+k*L(8),y0,L(3),0,360,clAqua,psSolid);
           Circle(Form, x0+k*L(24),y0,L(3),0,360,clAqua,psSolid);
           Pen.Width:=L(3);
           MoveTo(x0+k*L(12),y0);       LineTo(x0+k*L(20),y0);
           MoveTo(x0+k*L(28),y0-L(8));  LineTo(x0+k*L(28),y0+L(8));
         end;
    2: With Form.Canvas do                //������� �������������� �����
         begin
           Pen.Width:=L(1);                 Pen.Color:=clAqua;
           MoveTo(x0+k*L(6), y0+L(6));      LineTo(x0+k*L(11),y0-L(6));
           LineTo(x0+k*L(16),y0+L(6));      LineTo(x0+k*L(21),y0-L(6));
           LineTo(x0+k*L(26),y0+L(6));
           Pen.Width:=L(3);
           MoveTo(x0+k*L(28),y0-L(8));      LineTo(x0+k*L(28),y0+L(8));
         end;
  end;  {Typ}
END; {Draw_Support}
{-----------------------------------------------------------------------------}
PROCEDURE ScaleMove(MS: integer; var MasM: extended);
{����������� �������� ����������� ��� ��������� ���� ���������}
var k,m: integer;
    Mmin,Mmax,MaxMove: extended;
BEGIN
  Mmin:=1E20;  Mmax:=-1E20;
  for m:=1 to NE do
    for k:=0 to MS do
      begin
        if UL[m,k]<Mmin then Mmin:=UL[m,k];
        if UL[m,k]>Mmax then Mmax:=UL[m,k];
      end;
  if abs(Mmax)>abs(Mmin) then MaxMove:=abs(Mmax) else MaxMove:=abs(Mmin);
  MasM:=L(45)/MaxMove;
END;  {ScaleMove}
{-----------------------------------------------------------------------------}
PROCEDURE DrawForm(Form: TForm; MS: integer; Cc: TColor);
{��������� ����� ��������� �����������}
var m,k,Nk1,x1,y1,x2,y2: integer;
    Xk1,DL,MasM: extended;
BEGIN
  ScaleMove(MS,MasM);
  With Form.Canvas do
    begin Pen.Color:=Cc; Pen.Width:=1; end;

  for m:=1 to NE do
    begin
      DL:=EL[m].L/MS;
      Nk1:=EL[m].k1;
      Xk1:=US[Nk1].x;

      x1:=Xk +Round(Xk1*Mas);
      y1:=Yk -Round(UL[m,0]*MasM);
      for k:=1 to MS do
        begin
          x2:=Xk +Round((Xk1+k*DL)*Mas);
          y2:=Yk -Round(UL[m,k]*MasM);
          With Form.Canvas do
            begin MoveTo(x1,y1);   LineTo(x2,y2); end;
          x1:=x2;   y1:=y2;
        end;
    end;
END; {DrawForm}
{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}



end.
