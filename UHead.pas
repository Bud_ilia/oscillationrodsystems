unit UHead;
{-----------------------------------------------------------------------------}
                               interface
{-----------------------------------------------------------------------------}
uses  SysUtils, Graphics, Forms, StdCtrls, ExtCtrls, Dialogs, Menus,
      ComCtrls, Buttons, Classes, Controls, Grids, UGeneral;
{-----------------------------------------------------------------------------}
type
  TFHead = class(TForm)
    MainMenu1: TMainMenu;
    mData: TMenuItem;
    mInput: TMenuItem;
    mCorrectionData: TMenuItem;
    mCalc: TMenuItem;
    mForms: TMenuItem;
    mMoveNodes: TMenuItem;
    mExit: TMenuItem;
    bStart: TBitBtn;
    bStop: TBitBtn;

    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;

    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    OpenDialog1: TOpenDialog;
    StringGrid1: TStringGrid;
    ProgressBar1: TProgressBar;
    Label2: TLabel;
    Label3: TLabel;
    Label19: TLabel;
    ComboBox3: TComboBox;
    Label20: TLabel;
    ComboBox4: TComboBox;
    bOk2: TBitBtn;
    bOk1: TBitBtn;

    procedure mExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mCalcClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure mCorrectionDataClick(Sender: TObject);
    procedure mDisplacementClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mFormsClick(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure bOk2Click(Sender: TObject);
    procedure mInputClick(Sender: TObject);
    procedure mMoveNodesClick(Sender: TObject);
    procedure bOk1Click(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

PROCEDURE START;
PROCEDURE INIT_DATA;

var FHead: TFHead;
    PathCatalog: string;
    DXS,DYS,Xc,Yc: integer;
    MAS: extended;
{-----------------------------------------------------------------------------}
                            implementation
USES UTitul, ULib_Common, UInitData, UDisplacement, UFormsOfOscillation, UDrawing;
                              {$R *.dfm}
{-----------------------------------------------------------------------------}
var HW0,WW0,X00,Y00,NumberHeadScreen: integer;
    X01,X02,X03,Y01,Y02,Y03: integer;
{-----------------------------------------------------------------------------}
PROCEDURE START;
{������������� ������� ������, ���� �������}
var DS: extended;
BEGIN
  HW0:=Screen.Height;    WW0:=Screen.Width;
  MV:=Screen.Width/1920;    {=1}                    MVH:=HW0/1080;  {=1}
  NumberHeadScreen:=1;
  {���������� ������� ���������}
  PathCatalog:=ExtractFilePath(ParamStr(0));

  Xc:=L(1920) div 2;     Yc:=L(1080) div 2;
  DS:=L(800);
  {������� ���� �������}
  DXS:=L(DS);                   DYS:=L(DS);
  X00:=Xc -DXS div 2 -L(80);    Y00:=Yc -DYS div 2 -L(20);     //��������� ������� ����� ����� ����� � ��������� 20 pixel
  X01:=X00+DXS+L(160);          Y01:=Y00;
  X02:=X01;                     Y02:=Y01+DYS+L(40);
  X03:=X00;                     Y03:=Y02;
END; {START}
{-----------------------------------------------------------------------------}
PROCEDURE INIT_DATA;
{��������� ��������� �������� ������� �������� (���������)}
var i,j: integer;
BEGIN
  NU:=0;
  NE:=0;
  for i:=1 to NUmax do
    for j:=1 to NS do
      begin
        US[i].S1:=0;      //���������� ������ � �����
        US[i].S2:=0;
        Support[i,j]:=0;
      end;
  for i:=1 to NEmax do
    begin
      EL[i].E:=0;
      EL[i].F:=0;         //������� ������� ����������� ��������
      EL[i].p:=0;
      EL[i].T:=0;
      EL[i].Alfa:=0;
    end;
END;  {INIT_DATA}
{-----------------------------------------------------------------------------}
procedure TFHead.mCalcClick(Sender: TObject);
begin
  if Completion_Data then CALC
  else
    begin
      MessageDlg('�������� ������ ������� �� ���������',mtError,[mbOK],0);
      Exit;
    end;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.mFormsClick(Sender: TObject);
{����������� ���� ��������� ���������� ���������  m = 1, ...NE}
var NumUsel,NumSv: integer;
begin
  Omega  :=StrToFloat(FHead.Edit1.Text);
  NumUsel:=StrToInt(FHead.ComboBox3.Text);  //����� ����
  NumSv  :=StrToInt(FHead.ComboBox4.Text);  //���������� ����� �����  (��� ���������� ��������� NumSv=1)

  DeterminationOscillationShape(Omega,NumUsel,NumSv);

  DrawForm(FHead,MS,clLime);

  mMoveNodes.Enabled:=True;
  mForms.Enabled    :=False;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  Edit1.Text:=StringGrid1.Cells[ACol,ARow];
end;
{-----------------------------------------------------------------------------}
procedure TFHead.bOk2Click(Sender: TObject);
begin
  if (Edit1.Text='') or (ComboBox3.Text='') or (ComboBox4.Text='') then
    begin
      MessageDlg('�������� ������ ������� �� ���������',mtError,[mbOK],0);
      Exit;
    end;
  mForms.Enabled:=True;
  mCalc.Enabled:=False;
  mMoveNodes.Enabled:=False;
  Invalidate;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.mDisplacementClick(Sender: TObject);
begin
  FormTabl_Displacement;
  FDisplacement.ShowModal;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.mCorrectionDataClick(Sender: TObject);
begin
  if Completion_Data then
    begin
      FInitData.ShowModal;
      InputData;
      GEOMETRICS;
      Draw_Ferma(FHead,NU,NE);
    end
  else MessageDlg('�������� ������ ������� �� ���������',mtError,[mbOK],0);
end;
{-----------------------------------------------------------------------------}
procedure TFHead.FormCreate(Sender: TObject);
begin
  Width:=1920;    Height:=1080;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.FormPaint(Sender: TObject);
begin
  Draw_Ferma(FHead,NU,NE);
end;
{-----------------------------------------------------------------------------}
procedure TFHead.mExitClick(Sender: TObject);
begin
  Close;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FTitul.Close;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.mInputClick(Sender: TObject);
begin
  OpenDialog1.InitialDir:=PathCatalog +'DATA';
  OpenDialog1.Title:='�������� �������� ������ ������';
  if OpenDialog1.Execute then
    begin
      FormTablInitData_Excel;
      mMoveNodes.Enabled:=False;
      FInitData.ShowModal;
    end
  else Exit;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.mMoveNodesClick(Sender: TObject);
begin
  FormTabl_Displacement;
  FDisplacement.ShowModal;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.bOk1Click(Sender: TObject);
begin
  mCalc.Enabled     :=True;
  mForms.Enabled    :=False;
  mMoveNodes.Enabled:=False;
  Panel4.Visible    :=False;
  Panel6.Visible    :=False;
  Invalidate;
end;
{-----------------------------------------------------------------------------}
procedure TFHead.ComboBox3Change(Sender: TObject);
var N: integer;
begin
  N:=StrToInt(ComboBox3.Text);         //� ����
  ComboBox4.Text:='';
  ComboBox4.Items.Clear;
  if US[N].S1=0 then ComboBox4.Items.Add(IntToStr(1));
end;
{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}


end.


