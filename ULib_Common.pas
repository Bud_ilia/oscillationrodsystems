unit ULib_Common;
{-----------------------------------------------------------------------------}
                                 interface
{-----------------------------------------------------------------------------}
USES Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, Math, Series, Chart, TeEngine;
{-----------------------------------------------------------------------------}
CONST
  PD =2;   //6;      {������� ������� ���������������� ���������}
  NV =2;   //4;      {����� �������� � ������� ������� ������� ���. ���������}
  NP =4;             {����� ����� �������������� �� ������ ���������������}
  MS =100;           {����� ������� �������� (���������������)}    //MS+1  - ����� ����������� (�������) ���������� ������� � ���������� ��������

CONST
  NS   = 1;  //3;     //����� �������� ������� ����
  NEmax= 10{100};     //����������� ���������� ����� ���������
  NUmax= 11{100};     //����������� ���������� ����� �����
  Nmax = 11{300};     //Nmax = NUmax*Ns  - ����� �������� ������� ���� �������

TYPE
  Vector = array [1..PD] of extended;    //U, u
  MMY    = array [1..NV] of Vector;  {������� Y}
  MM_Y   = array [0..MS] of Vector;
  MMMZ   = array [0..NP] of MMY;                  {������ ������� ������� ���. ���������}
  MMA    = array [1..NV-1, 1..NV-1] of extended;  {������� ������� �������� ���������}
  VB     = array [1..NV-1]  of extended;          {������ ������ ����� ������� �������� ���������}
  MMW    = array [1..NV-1, 1..NV]   of extended;
  MMM_Z  = array [0..MS]  of MMY;
  MMM_W  = array [1..MS-1]  of MMW;

TYPE
  M_2NS = array [1..2*NS, 1..2*NS] of extended;
  M_NS  = array [1..NS, 1..NS]     of extended;
  V_NS  = array [1..NS]            of extended;
  V_2NS = array [1..2*NS]          of extended;
  M_H = array [1..Nmax, 1..Nmax]   of extended;    // = array [1..NUmax*NS,1..NUmax*NS] of extended
  V_H = array [1..Nmax]            of extended;    // = array [1..NUmax*NS] of extended

  procRKT   = procedure(Omega: extended;  m: integer; x: extended; Y: MMY;  var F: MMY);
  FunSearch = function(Omega: extended): extended;


PROCEDURE Circle(Formg: TForm;   x0,y0,R: integer;  F0,Fk: single; Color: TColor;  Style: TPenStyle);
FUNCTION  CONVERT(A: single; LA: integer; Prec,Digit: integer): string;
FUNCTION  COSD(X: extended): extended;
FUNCTION  L(Ls: single): integer;

PROCEDURE GAUSS(N: integer;  A: M_H;  B: V_H;  var X: V_H);
PROCEDURE U_MV(N: integer; A: M_2NS; V: V_2NS; var V1: V_2NS);
PROCEDURE U_MM(N: integer; A,B: M_2NS;  var C: M_2NS);
FUNCTION  SIND(X: extended): extended;
PROCEDURE TRANS_MM(N: integer; A: M_2NS; var AT: M_2NS);
PROCEDURE DETERMINANT(N: integer; var A: M_H;  var Det: extended);
PROCEDURE RKT(Omega: extended; m: integer; x0,L: extended; NP: integer; Y0: MMY; F: procRKT; var Y: MMY);
PROCEDURE DICHOTOMY(A1,B1,Eps,Eps1: extended; FUN: FunSearch;  var X: extended; var RESULT: boolean);
{-----------------------------------------------------------------------------}
                              implementation
USES UGeneral;
{-----------------------------------------------------------------------------}
//var MV,MVH: single;   //MV  - ������� ����������� �� ����������� =Screen.Width/1920;
                        //MVH - ������� ����������� �� ���������   =Screen.Height/1080;
{-----------------------------------------------------------------------------}
PROCEDURE Circle(Formg: TForm;   x0,y0,R: integer;  F0,Fk: single;
                 Color: TColor;  Style: TPenStyle);
var x1,y1,x2,y2,x3,y3,x4,y4: integer;
begin
  x1:=x0-R;                     y1:=y0-R;
  x2:=x0+R;                     y2:=y0+R;
  x3:=x0+round(R*SIND(F0));     y3:=y0+round(R*COSD(F0));
  x4:=x0+round(R*SIND(Fk));     y4:=y0+round(R*COSD(Fk));
  With Formg.Canvas do
    begin
      Pen.Color:=Color;    Pen.Style:=Style;
      Arc(x1,y1,x2,y2,x3,y3,x4,y4);
    end;
end; {Circle}
{-----------------------------------------------------------------------------}
FUNCTION CONVERT(A: single; LA: integer; Prec,Digit: integer): string;
{LA    - ����� ����� � (���������� �������� + ���������� ���������);
 Prec  - �������� ������������� (���������� ���������);
 Digit - ���������� ���� ����� �������}
var SA  : string;
    i,L1: integer;
BEGIN
  SA:=FloatToStrF(A,ffFixed,Prec,Digit);
  L1:=Length(SA);
  Result:='';
  for i:=1 to LA-L1 do Result:=Result +' ';
  Result:=Result + SA;
END; {CONVERT}
{-----------------------------------------------------------------------------}
FUNCTION COSD(X: extended): extended;
BEGIN {COSD}
  X:=X*pi/180;   COSD:=Cos(X);
END; {COSD}
{-----------------------------------------------------------------------------}
FUNCTION L(Ls: single): integer;
{��������������� ����������� � ����������� �� ���������� ������;
 MV:=Screen.Width/1920;
 Ls - ������ ����������� � �������� ��� ������ ������  Screen.Width = 1920}
begin
  Result:=round(MV*Ls);
end; {L}
{-----------------------------------------------------------------------------}
PROCEDURE GAUSS(N: integer;  A: M_H;  B: V_H;  var X: V_H);
{������� ������� �������� �����p�������� �p�������  AX=B ������� ������
 � ������� �������� ��������.
 N - ������� �������,
 A[1..N,1..N] - ���p��� �������,
 �[1..N] - �����p �p���� ����� �������,
 X[1..N] - �����p p������ �������}
var U,W     : extended;
    i,j,k,m : integer;
BEGIN
  for i:=1 to N do
    begin
      W:=0;  m:=i;
      for k:=i to N do
        if abs(A[k,i]) > abs(W) then begin W:=A[k,i]; m:=k end;

      if W=0 then
        begin
          MessageDlg('������������ ������� ����� ����'+#13
                     +'��������� ��������� ������',mtInformation,[mbOK],0);
          Halt;
        end;

      A[m,i]:=A[i,i];
      for j:=i+1 to N do
        begin
          U:=A[m,j]/W;   A[m,j]:=A[i,j];   A[i,j]:=U;
          for k:=i+1 to N do  A[k,j]:=A[k,j]-A[k,i]*U
        end;
      U:=B[m]/W;   B[m]:=B[i];   B[i]:=U;
      for k:=i+1 to N do  B[k]:=B[k]-A[k,i]*U;
    end;   {i}

    X[N]:=B[N];
    for i:=N-1 downto 1 do
      begin
        X[i]:=B[i];
        for j:=i+1 to N do  X[i]:=X[i]-A[i,j]*X[j]
      end
END;  {GAUSS}
{-----------------------------------------------------------------------------}
FUNCTION SIND(X: extended): extended;
BEGIN {SIND}
  X:=X*pi/180;   SIND:=Sin(X);
END; { SIND }
{-----------------------------------------------------------------------------}
PROCEDURE U_MV(N: integer; A: M_2NS; V: V_2NS; var V1: V_2NS);
{��������� ���������� ������� �� ������}
var i,j: integer;
BEGIN
  for i:=1 to N do
    begin
      V1[i]:=0;
      for j:=1 to N do  V1[i]:=V1[i] +A[i,j]*V[j];
    end;
END;  {U_MV}
{-----------------------------------------------------------------------------}
PROCEDURE U_MM(N: integer; A,B: M_2NS;  var C: M_2NS);
{��������� ���������� ������}
var i,j,k: integer;
BEGIN
  for i:=1 to N do
    for j:=1 to N do
      begin
        C[i,j]:=0;
        for k:=1 to N do C[i,j]:=C[i,j] +A[i,k]*B[k,j];
      end;
END;  {U_MM} 
{-----------------------------------------------------------------------------}
PROCEDURE U_VV(N: integer;  V1,V2: V_H; var VV: extended);
{��������� ��������� ������� �� ������}
var i: integer;
BEGIN
  VV:=0;
  for i:=1 to N do VV:=VV +V1[i]*V2[i];
END;  {U_VV}
{-----------------------------------------------------------------------------}
PROCEDURE TRANS_MM(N: integer; A: M_2NS; var AT: M_2NS);
{���������������� ���������� ������� A � ������� AT}
var i,j: integer;
BEGIN
  for i:=1 to N do
    for j:=1 to N do
      AT[i,j]:=A[j,i];
END;  {TRANS_MM}
{-----------------------------------------------------------------------------}
PROCEDURE DETERMINANT(N: integer; var A: M_H;  var Det: extended);
var Max,T,D : extended;
    i,j,k,Imax : integer;
    Label FIN;
BEGIN
  D:=1;        Imax:=1;
  for k:=1 to N do
    begin
      Max:=0;
      for i:=k to N do                              //����� �������� �������� � k-�� �������
        begin
          T:=A[i,k];
          if ABS(T)>ABS(Max) then
            begin Max:=T; Imax:=i; end;             //������� ������� ������ �  j-� ������ k-�� �������
        end; {i}
      if Max=0 then begin D:=0; Goto FIN; end;      //� j-�� ������� ��� �������� �������
      if Imax<>k then
        begin
          D:=-D;                                    //������ ������� k-� � j-� ������
          for j:=1 to N do
            begin T:=A[Imax,j]; A[Imax,j]:=A[k,j];  A[k,j]:=T; end;
        end;
      for i:=k+1 to N do
        begin                   //� ��������� (k+1)...N ����� ���������� �������� i-� ������, ���������� �� -A[i,k]/A[k,k]
          T:=-A[i,k]/Max;
          for j:=1 to N do A[i,j]:=A[i,j]+T*A[k,j];
        end; {i}
      D:=D*A[k,k];
    end; {k}
FIN: Det:=D;
END; {DETERMINANT}
{-----------------------------------------------------------------------------}
PROCEDURE RKT(Omega: extended; m: integer; x0,L: extended; NP: integer; Y0: MMY;
              F: procRKT; var Y: MMY);
{��������� ��������� Y � ����� ��������� �������������� [x0,x0+L]}
{�����p�p������ ������� �����p���������x �p�������  dY/dx=F(x,Y) ��� m-�� ��������}
    { m      - ����� ����������� ��������;
      x0     - ��������� ����� ��������� ��������������;
      L      - ����� ����p���� �����p�p������;
      NP     - ����� ������� ����� �� ��������� ��������������;
      Y0     - ������ ��������x �������� �������;
      Y      - ������ �������� � ����� ��������� ��������������;
      procF  = procedure(m: integer; x: extended; Y: MMY;  var F: MMY)
      F      - ������ ����������� �������;
      Y      - ������ �������� � ������ �����;
      MMY    = array [1..NV] of Vector;
      Vector = array [1..PD] of extended;
      PD     - ��p���� ������� ���.���������;
      NV     - ����� �������� � ������� Y)
    }
var K1,K2,K3,K4,Y1: MMY;
    x,dx          : extended;
    i,j,k         : integer;
BEGIN {RKT}
  dx:=L/NP;   x:=x0;   Y:=Y0;
  for k:=0 to NP-1 do
    begin
      F(Omega,m,x,Y,K1);
      x:=x+dx/2;
      for i:=1 to NV do
        for j:=1 to PD do Y1[i,j]:=Y[i,j]+K1[i,j]*dx/2;
      F(Omega,m,x,Y1,K2);
      for i:=1 to NV do
        for j:=1 to PD do Y1[i,j]:=Y[i,j]+K2[i,j]*dx/2;
      F(Omega,m,x,Y1,K3);
      x:=x+dx/2;
      for i:=1 to NV do
        for j:=1 to PD do Y1[i,j]:=Y[i,j]+K3[i,j]*dx;
      F(Omega,m,x,Y1,K4);
      for i:=1 to NV do
        for j:=1 to PD do
          Y[i,j]:=Y[i,j]+(K1[i,j]+2*K2[i,j]+2*K3[i,j]+K4[i,j])*dx/6;
    end;
END;   {RKT}
{------------------------------------------------------------------------------}
PROCEDURE DICHOTOMY(A1,B1,Eps,Eps1: extended; FUN: FunSearch;  var X: extended; var RESULT: boolean);
{����� ����� X ������� FUN �� ������� [A1,B1] � ��������� Eps.
 ����� ������������ ��� ���������� ������� |b-a|<Eps1.
 ��� �������� ������ RESULT=True
}
var a,b,c,fa,fb,fc: extended;
Label Iter,Fin;
BEGIN
  RESULT:=False;
  a:=A1;       b:=B1;
  fa:=FUN(a);    if ABS(fa)<Eps then begin X:=a; Goto Fin;  end;
  fb:=FUN(b);    if ABS(fb)<Eps then begin X:=b; Goto Fin;  end;
  if SIGN(fa)=SIGN(fb)        then Exit;
                                      
Iter: c:=(a+b)/2;
      fc:=FUN(c);
      if ABS(fc)<Eps       then begin X:=c; Goto Fin;  end;
      if SIGN(fc)=SIGN(fa) then begin a:=c; fa:=fc; end 
                           else b:=c;
      if ABS(b-a)<Eps1     then X:=c else Goto  Iter;

Fin:  RESULT:=True;
END;  {DICHOTOMY}
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}


end.

