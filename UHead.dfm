object FHead: TFHead
  Left = 396
  Top = 170
  BorderIcons = [biSystemMenu]
  Caption = #1050#1086#1083#1077#1073#1072#1085#1080#1103' '#1089#1090#1077#1088#1078#1085#1077#1074#1099#1093' '#1089#1080#1089#1090#1077#1084
  ClientHeight = 642
  ClientWidth = 1095
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1095
    Height = 30
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 860
    Top = 36
    Width = 110
    Height = 108
    BevelInner = bvLowered
    Color = clGray
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 14
      Top = 8
      Width = 83
      Height = 19
      Caption = #1040#1085#1080#1084#1072#1094#1080#1103
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clAqua
      Font.Height = -17
      Font.Name = 'ARIAL CYR'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object bStart: TBitBtn
      Left = 20
      Top = 40
      Width = 70
      Height = 25
      Caption = #1055#1091#1089#1082' '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'ARIAL CYR'
      Font.Style = [fsBold]
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033BBBBBBBBBB
        BB33337777777777777F33BB00BBBBBBBB33337F77333333F37F33BB0BBBBBB0
        BB33337F73F33337FF7F33BBB0BBBB000B33337F37FF3377737F33BBB00BB00B
        BB33337F377F3773337F33BBBB0B00BBBB33337F337F7733337F33BBBB000BBB
        BB33337F33777F33337F33EEEE000EEEEE33337F3F777FFF337F33EE0E80000E
        EE33337F73F77773337F33EEE0800EEEEE33337F37377F33337F33EEEE000EEE
        EE33337F33777F33337F33EEEEE00EEEEE33337F33377FF3337F33EEEEEE00EE
        EE33337F333377F3337F33EEEEEE00EEEE33337F33337733337F33EEEEEEEEEE
        EE33337FFFFFFFFFFF7F33EEEEEEEEEEEE333377777777777773}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
    end
    object bStop: TBitBtn
      Left = 20
      Top = 70
      Width = 70
      Height = 25
      Caption = #1057#1090#1086#1087' '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'ARIAL CYR'
      Font.Style = [fsBold]
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
        3333333777333777FF3333993333339993333377FF3333377FF3399993333339
        993337777FF3333377F3393999333333993337F777FF333337FF993399933333
        399377F3777FF333377F993339993333399377F33777FF33377F993333999333
        399377F333777FF3377F993333399933399377F3333777FF377F993333339993
        399377FF3333777FF7733993333339993933373FF3333777F7F3399933333399
        99333773FF3333777733339993333339933333773FFFFFF77333333999999999
        3333333777333777333333333999993333333333377777333333}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel3: TPanel
    Left = 8
    Top = 80
    Width = 180
    Height = 216
    BevelInner = bvLowered
    TabOrder = 2
    Visible = False
    object Label7: TLabel
      Left = 24
      Top = 75
      Width = 15
      Height = 24
      Caption = 'w'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 40
      Top = 93
      Width = 21
      Height = 17
      Caption = 'min'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 24
      Top = 125
      Width = 15
      Height = 24
      Caption = 'w'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 40
      Top = 142
      Width = 25
      Height = 17
      Caption = 'max'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 25
      Top = 10
      Width = 131
      Height = 18
      Caption = #1059#1082#1072#1078#1080#1090#1077' '#1076#1080#1072#1087#1072#1079#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 38
      Top = 28
      Width = 104
      Height = 18
      Caption = #1087#1086#1080#1089#1082#1072' '#1095#1072#1089#1090#1086#1090
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 10
      Top = 46
      Width = 160
      Height = 18
      Caption = #1089#1086#1073#1089#1090#1074#1077#1085#1085#1099#1093' '#1082#1086#1083#1077#1073#1072#1085#1080#1081
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object ComboBox1: TComboBox
      Left = 80
      Top = 82
      Width = 80
      Height = 24
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = '0'
      Items.Strings = (
        '0'
        '100'
        '1000'
        '10000'
        '20000'
        '30000')
    end
    object ComboBox2: TComboBox
      Left = 80
      Top = 132
      Width = 80
      Height = 24
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = '20000'
      Items.Strings = (
        '100'
        '1000'
        '10000'
        '20000'
        '30000'
        '50000')
    end
    object bOk1: TBitBtn
      Left = 41
      Top = 180
      Width = 100
      Height = 25
      Caption = 'Ok'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clGreen
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
      TabOrder = 2
      OnClick = bOk1Click
    end
  end
  object Panel4: TPanel
    Left = 8
    Top = 302
    Width = 180
    Height = 297
    BevelInner = bvLowered
    TabOrder = 3
    Visible = False
    object Label13: TLabel
      Left = 32
      Top = 16
      Width = 123
      Height = 18
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1095#1072#1089#1090#1086#1090#1091
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clCream
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 12
      Top = 34
      Width = 160
      Height = 18
      Caption = #1089#1086#1073#1089#1090#1074#1077#1085#1085#1099#1093' '#1082#1086#1083#1077#1073#1072#1085#1080#1081
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 32
      Top = 63
      Width = 15
      Height = 24
      Caption = 'w'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -21
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 48
      Top = 70
      Width = 9
      Height = 21
      Caption = '*'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 32
      Top = 120
      Width = 118
      Height = 18
      Caption = #1059#1082#1072#1078#1080#1090#1077' '#8470' '#1091#1079#1083#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 30
      Top = 138
      Width = 122
      Height = 18
      Caption = #1080' '#8470' '#1085#1091#1083#1077#1074#1086#1081' '#1089#1074#1103#1079#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 14
      Top = 171
      Width = 51
      Height = 19
      Caption = #8470' '#1091#1079#1083#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 14
      Top = 221
      Width = 60
      Height = 19
      Caption = #8470' '#1089#1074#1103#1079#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 72
      Top = 72
      Width = 80
      Height = 24
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object ComboBox3: TComboBox
      Left = 82
      Top = 168
      Width = 80
      Height = 24
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = ComboBox3Change
    end
    object ComboBox4: TComboBox
      Left = 82
      Top = 218
      Width = 80
      Height = 24
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Items.Strings = (
        '')
    end
    object bOk2: TBitBtn
      Left = 41
      Top = 264
      Width = 100
      Height = 25
      Caption = 'Ok'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clGreen
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
      TabOrder = 3
      OnClick = bOk2Click
    end
  end
  object Panel6: TPanel
    Left = 194
    Top = 258
    Width = 200
    Height = 347
    BevelInner = bvRaised
    BevelWidth = 2
    BorderWidth = 2
    Locked = True
    TabOrder = 4
    Visible = False
    object Label22: TLabel
      Left = 63
      Top = 12
      Width = 64
      Height = 19
      Caption = #1063#1072#1089#1090#1086#1090#1099
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 11
      Top = 27
      Width = 175
      Height = 19
      Caption = #1089#1086#1073#1089#1090#1074#1077#1085#1085#1099#1093' '#1082#1086#1083#1077#1073#1072#1085#1080#1081
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label24: TLabel
      Left = 16
      Top = 46
      Width = 165
      Height = 17
      Caption = #1074' '#1076#1080#1072#1087#1072#1079#1086#1085#1077' [0 ... 10000]'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clSkyBlue
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object StringGrid1: TStringGrid
      Left = 3
      Top = 75
      Width = 190
      Height = 262
      ColCount = 2
      DefaultColWidth = 91
      DefaultRowHeight = 20
      FixedColor = clSilver
      FixedCols = 0
      RowCount = 12
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnSelectCell = StringGrid1SelectCell
    end
  end
  object Panel7: TPanel
    Left = 750
    Top = 80
    Width = 350
    Height = 24
    BevelOuter = bvNone
    Color = clYellow
    TabOrder = 5
    Visible = False
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 0
      Width = 350
      Height = 24
      Align = alClient
      Enabled = False
      Step = 5
      TabOrder = 0
    end
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 40
    object mData: TMenuItem
      Caption = #1048#1089#1093#1086#1076#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      object mInput: TMenuItem
        Caption = #1042#1074#1086#1076
        OnClick = mInputClick
      end
      object mCorrectionData: TMenuItem
        Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1082#1072
        OnClick = mCorrectionDataClick
      end
    end
    object mCalc: TMenuItem
      Caption = #1063#1072#1089#1090#1086#1090#1099' '#1082#1086#1083#1077#1073#1072#1085#1080#1081
      Enabled = False
      OnClick = mCalcClick
    end
    object mForms: TMenuItem
      Caption = #1060#1086#1088#1084#1099' '#1082#1086#1083#1077#1073#1072#1085#1080#1081
      Enabled = False
      OnClick = mFormsClick
    end
    object mMoveNodes: TMenuItem
      Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103' '#1091#1079#1083#1086#1074
      Enabled = False
      OnClick = mMoveNodesClick
    end
    object mExit: TMenuItem
      Caption = #1042#1099#1093#1086#1076
      OnClick = mExitClick
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '.xls'
    Filter = #1050#1085#1080#1075#1072' Microsoft Exce [*.xls]|*.xls'
    Left = 56
    Top = 40
  end
end
