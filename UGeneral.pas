unit UGeneral;
{-----------------------------------------------------------------------------}
                               interface
{-----------------------------------------------------------------------------}
USES Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, ULib_Common;
{-----------------------------------------------------------------------------}
CONST
  NF =10;             //��������� ����� ������ ����������� ���������

TYPE
  V_NF   = array [1..NF] of extended;

TYPE
  Usel = Record
    x: extended;
    y: extended;      //x,y - ���������� ����
    S1: integer;      //��������� ������� - ������� ������� ����� � ����: S1 =1;  ��c������� ������� �����: S1 =0
    S2: integer;      //��������� ������� - ������� ������� ����� � ����: S2 =1;  ��c������� ������� �����: S2 =0
    G1: extended;     //����������� ��������� ������� ����� � ���� i (i < j), �/��
    G2: extended;     //����������� ��������� ������� ����� � ���� j,(i < j), �/��
    D: V_NS;          //Dx - ������� ����������� � ���������� ����������� - �����������
  end;

  Element = Record
    k1: integer;
    k2: integer;      //k1,k2 - ������ ����� �������� (k2>k1)
    E:  extended;
    F:  extended;        //������� �������
    p:  extended;
    alfa: extended;
    T:  extended;
    Dens: extended;      //��������� ���������������� ���������, �*�2/��4

    L:  extended;        //����� ��������, ��
    Sf: extended;
    Cf: extended;
    KG:  M_2NS;
    K11: M_NS;
    K12: M_NS;
    K21: M_NS;
    K22: M_NS;
    DL : V_2NS;       //������ ����������� ����� �������� (� ��������� �����������)
    c: extended;      //����������� ��������� ������� �����, �/��
  end;

PROCEDURE CALC;
PROCEDURE FORM_KL(Omega: extended; m: integer; var KL: M_2NS);
PROCEDURE FORM_MT(m: integer; var MT: M_2NS);
PROCEDURE FORM_KG(Omega: extended; m: integer; var KG: M_2NS);
PROCEDURE Form_Kij(m: integer; KG: M_2NS;  var K11,K12,K21,K22: M_NS);
PROCEDURE FORM_MGlobal(var MG: M_H);
PROCEDURE Boundary_Data(MG: M_H; var MGmod: M_H);
PROCEDURE Rightmember_0(Omega: extended;  m: integer; x: extended; Y: MMY;  var DY_DS: MMY);
FUNCTION  FUN(Omega: extended): extended;
PROCEDURE SEARCHING(Omega1,Omega2,EpsD,Eps: extended; Nstep: integer; FUN: FunSearch;  var NFreal: integer;  var OmegaK: V_NF);
PROCEDURE FormTabl_OmegaK;

var NU,NE: integer;         //NU - ����� ����� ���� �����������; NE - ����� ���������� ��������� ����������� �����������
    US: array [1..NUmax] of Usel;
    EL: array [1..NEmax] of Element;
    MG,MGmod: M_H;          {���������� ������� ��������� �������:    M_H = array [1..Hmax, 1..Hmax] of extended;
                                 MG - ��� ����� ��������� �������,    //Hmax = NUmax*Ns  - ����� �������� ������� ���� �������
                                 MGmod - � ������ ��������� �������}

    DLT: V_H;              //���������� ������� ����������� ����� ���������� �����������
    Completion_Data: Boolean;
    Support: array[1..NUmax,0..3] of extended;
    NSupport: integer;    //���������� ���� (����� �� ������� �������� �����)
    MV: extended;         //MV:=Screen.Width/1920;
    MVH: extended;        //MVH:=Screen.Height/1080;

    Det: extended;                     //������������ ����������� ������� �������� �������������� ���������
    YS: MM_Y;                          //MM_Y  = array [0..MS] of Vector;
    UL,UG: array [1..NEmax,0..MS] of extended;     //����������� ���������� ������� ���������� ��������� (��������� � ����������)
    Omega: extended;                  //�������� ������� ���������
    OmegaK: V_NF;                     //������ ������ ����������� ��������� �������
    DX : array [1..MS] of extended;   //������ ���� �������� ���������������
    Y00: MMY;                         //�������, � ������� ��� �������� =0,����� Y00[1,1]=1
    Z  : MMM_Z;                       //MMM_Z = array [0..MS]  of MMY;
{-----------------------------------------------------------------------------}
                            implementation
USES UHead, UInitData;
{-----------------------------------------------------------------------------}
var NFreal: integer;        //�������������� ����� ������ ����������� ��������� � �������� ���������
{-----------------------------------------------------------------------------}
PROCEDURE CALC;
var i,j,Nstep: integer;
    Omega1,Omega2,EpsD,Eps: extended;
BEGIN
  {��������� ��������� ������� ��� ������� ����� ����}
  for i:= 1 to NV do for j:= 1 to PD do Y00[i,j]:=0;
  for i:= 1 to NV-1 do Y00[i,i]:=1;

  Omega1:=StrToFloat(FHead.ComboBox1.Text);
  Omega2:=StrToFloat(FHead.ComboBox2.Text);
  EpsD:=1;        Eps:=0.01;
  Nstep:=100;
  {������� ��� �������� ��������� Omega* � �������� ��������� ��� ���������,
   ��� ������� ������������ Det=0, �.�. ����������� ������� ��������� �����������}
  FHead.ProgressBar1.Position:=0;    FHead.ProgressBar1.Max:=100;
  FHead.Panel7.Visible:=True;
  SEARCHING(Omega1,Omega2,EpsD,Eps,Nstep, FUN, NFreal, OmegaK);

  FHead.ProgressBar1.Position:=FHead.ProgressBar1.Max;
  FHead.Panel7.Visible:=False;
  if NFreal >0 then
    begin
      MessageDlg('��������� ������ ������� �������� !', mtInformation,[mbOK],0);
      FormTabl_OmegaK;
      FHead.Panel4.Visible:=True;
      FHead.Panel6.Visible:=True;
    end
  else MessageDlg('� ��������� [' + FHead.ComboBox1.Text + ' ... ' + FHead.ComboBox2.Text + ']' +#13
                  +'����������� ������� �� ����������', mtInformation,[mbOK],0);

  FHead.mCalc.Enabled:=False;
END;  {CALC}
{-----------------------------------------------------------------------------}
FUNCTION FUN(Omega: extended): extended;
{������� FUN ��������� ������������ (�����������) ���������� ������� ��������� ���������� �������}
var m: integer;
    KG: M_2NS;
BEGIN
  {��������� ���������� ������� ��������� ���������� ���������}
  for m:=1 to NE do
    begin
      FORM_KG(Omega,m,KG);
      EL[m].KG:=KG;
    end;

  {��������� ���������� ��������� ���������� ���������}
  for m:=1 to NE do
    Form_Kij(m,EL[m].KG, EL[m].K11,EL[m].K12,EL[m].K21,EL[m].K22);

  {��������� ���������� ������� ��������� �������   MG}
  FORM_MGlobal(MG);                     //FormTabl_00(0,0,MG);     //MG

  {������������ ���������� ������� ������� ��������� � ������ �����������, ���������� �� ����������� ����� �����������  MGmod}
  Boundary_Data(MG, MGmod);             //FormTabl_00(1*(NU*NS+1),0,MGmod);      //MGmod

  {��������� ������������ ������� MGmod ��� ��������� �������� ��������� Omega}
  DETERMINANT(NU*NS,MGmod, Det);       //FormTabl_00(2*(NU*NS+1),0,MGmod);       //����������� �������
  FUN:=Det;
END; {FUN}
{-----------------------------------------------------------------------------}
PROCEDURE SEARCHING(Omega1,Omega2,EpsD,Eps: extended; Nstep: integer; FUN: FunSearch;  var NFreal: integer; var OmegaK: V_NF);
var i,k,Istep: integer;
    a,b,Omega,DOmega: extended;
    RESULT: Boolean;
    Label Fin,Iter;
BEGIN
  for i:=1 to NF do OmegaK[i]:=0;
  RESULT:=False;
  DOmega:=(Omega2-Omega1)/Nstep;
  k:=1;
  for Istep:=0 to Nstep do
    begin
      a:=Istep*DOmega;  b:=a+DOmega;
      Omega:=0;
      DICHOTOMY(a,b,EpsD,Eps, FUN, Omega,RESULT);
      if RESULT then begin OmegaK[k]:=Omega; NFreal:=k;  INC(k); end;
      FHead.ProgressBar1.Position:=k*10;
    end; {Istep}

END; {Searching}
{-----------------------------------------------------------------------------}
PROCEDURE FORM_KL(Omega: extended;  m: integer; var KL: M_2NS);
{������������ ��������� ������� ��������� ��� ����������� �������� m}
var Y0,Y: MMY;
    X0  : extended;
    i,j,KW : integer;
BEGIN
  {�������� �������}
  for i:=1 to 2*NS do
    for j:=1 to 2*NS do KL[i,j]:=0;

  {��������� ������� ��������� ������� Y0.
   ������� ����� � ����, ��� �������� ������� ������� ������� Y ������������ �������������� �������,
   ��� ���  MMY=array [1..NV] of Vector}
  Y0:=Y00;
  Y0[2,2]:=1;

  {��������� ������� ���� ����� ���� � �������� ������� ������� � ����� ����������� �������� x=L.
   ��� ������ ����������}
  for KW:=1 to MS do
    begin
      X0:=(KW-1)*DX[m];
      RKT(Omega,m,X0,DX[m],NP,Y0,RightMember_0, Y);
      Y0:=Y;
    end;
    
  {��������� ��������� ������� ��������� ����������� ��������}
  KL[1,1]:=Y[2,2]/Y[1,2];                    KL[1,2]:=-1/Y[1,2];
  KL[2,1]:=-(Y[2,2]*Y[1,1])/Y[1,2]+Y[2,1];   KL[2,2]:=Y[1,1]/Y[1,2];
END;    {FORM_KL}
{-----------------------------------------------------------------------------}
PROCEDURE FORM_MT(m: integer; var MT: M_2NS);
{������������ ������� �������� � ��������� ����������� ��� ����������� �������� m}
var i,j: integer;
BEGIN
  for i:=1 to 2*NS do
    for j:=1 to 2*NS do MT[i,j]:=0;
  With EL[m] do
    begin
      MT[1,1]:=Cf;
      MT[2,2]:=Cf;
    end;
END;  {FORM_MT}
{-----------------------------------------------------------------------------}
PROCEDURE FORM_KG(Omega: extended; m: integer; var KG: M_2NS);
{������������ ���������� ������� ��������� ��� ����������� �������� m}
var KL,MT,M1,MT_T: M_2NS;
BEGIN
  {��������� ��������� ������� ��������� ����������� ��������}
  FORM_KL(Omega,m,KL);

  {��������� ������� �������� � ��������� ����������� ��� ����������� ��������}
  FORM_MT(m,MT);

  {��������� ����������������� ������� �������� � ���������� �����������}
  Trans_MM(2*NS,MT, MT_T);
  
  {��������� ������� ��������� ����������� �������� � ���������� �����������}
  U_MM(2*NS,KL,MT, M1);
  U_MM(2*NS,MT_T,M1, KG);
END;  {FORM_KG}
{-----------------------------------------------------------------------------}
PROCEDURE Form_Kij(m: integer; KG: M_2NS;  var K11,K12,K21,K22: M_NS);
{������������ ��������� Kij ���������� ������� ��������� ��� ����������� �������� m}
var i,j: integer;
BEGIN
  for i:=1 to Ns do
    for j:=1 to Ns do
      begin
        K11[i,j]:=KG[i,j];          K12[i,j]:=KG[i,j+NS];
        K21[i,j]:=KG[i+NS,j];       K22[i,j]:=KG[i+NS,j+NS];
      end;
END;  {Form_Kij}
{-----------------------------------------------------------------------------}
PROCEDURE FORM_MGlobal(var MG: M_H);
{������������ ���������� ������� ��������� �������-���������� �������.
 ����� ����� ����� ����� �������� = NU*NS}
var i,j,k,k1,k2,m,n: integer;
BEGIN
  k:=NU*NS;
  for i:=1 to k do
    for j:=1 to k do  MG[i,j]:=0;

  for i:=1 to NE do
    begin
      k1:=EL[i].k1;    k2:=EL[i].k2;
      for m:=1 to NS do
        for n:=1 to NS do
          begin
            MG[(k1-1)*NS+m,(k1-1)*NS+n]:=MG[(k1-1)*NS+m,(k1-1)*NS+n] +EL[i].K11[m,n];
            MG[(k1-1)*NS+m,(k2-1)*NS+n]:=MG[(k1-1)*NS+m,(k2-1)*NS+n] +EL[i].K12[m,n];
            MG[(k2-1)*NS+m,(k1-1)*NS+n]:=MG[(k2-1)*NS+m,(k1-1)*NS+n] +EL[i].K21[m,n];
            MG[(k2-1)*NS+m,(k2-1)*NS+n]:=MG[(k2-1)*NS+m,(k2-1)*NS+n] +EL[i].K22[m,n];
          end;
    end;
END;  {FORM_MGlobal}
{-----------------------------------------------------------------------------}
PROCEDURE Boundary_Data (MG: M_H; var MGmod: M_H);
{���� �����������, ���������� �� ����������� ����� �����������/
 ������������� ���������� ������� ���������}
var i: integer;
BEGIN
  MGmod:=MG;

  if US[1].S1=1 then              //������� ����� � 1 ����
    begin
      for i:=1 to NU do MGmod[1,i]:=0;
      MGmod[1,1]:=1;
    end;
  if US[NU].S1=1 then             //������� ����� � NU ����
    begin
      for i:=1 to NU do MGmod[NU,i]:=0;
      MGmod[NU,NU]:=1;
    end;
  if US[1].S2=1  then MGmod[1,1]:=MGmod[1,1] -US[1].G1;           //������� ����� � 1  ����
  if US[NU].S2=1 then MGmod[NU,NU]:=MGmod[NU,NU] +US[NU].G2;      //������� ����� � NU ����
END;  {Boundary_Data}
{-----------------------------------------------------------------------------}
PROCEDURE RightMember_0(Omega: extended; m: integer; x: extended; Y: MMY;  var DY_DS: MMY);
{��������� ���������� ������ ����� ���������� ������� ���������������� ���������}
var WRS,ES: extended;
    i: integer;
BEGIN
  WRS:=SQR(Omega)*El[m].DENS*El[m].F;
  ES :=El[m].E*El[m].F;
  {���� ���������� ���������}
  for i:=1 to NV do
    begin
      DY_DS[i,1]:= -WRS*Y[i,2];     //N
      DY_DS[i,2]:= Y[i,1]/ES;       //u
    end;
END; {RightMember_0}
{-----------------------------------------------------------------------------}
PROCEDURE FormTabl_OmegaK;         //������������ ������� ������ ����������� ���������
var IRow,k: integer;
BEGIN
  FHead.Label24.Caption:='� ��������� [' + FHead.ComboBox1.Text + ' ... ' + FHead.ComboBox2.Text + ']';
  With FHead.StringGrid1 do
    begin
      RowCount:=NFreal+1;
      {������� �������}
      for IRow:=1 to RowCount do
        for k:=0 to ColCount do Cells[k,IRow]:='';

      Cells[0,0]:='              k';     Cells[1,0]:='   OmegaK, 1/c';

      for IRow:=1 to NFreal do
        begin
          Cells[0,IRow]:='              ' + IntToStr(IRow);
          Cells[1,IRow]:=CONVERT(OmegaK[IRow],9,8,3);
        end;
    end;

  FHead.StringGrid1.Height:=FHead.StringGrid1.RowCount*21 +10;
  FHead.Panel6.Height:=FHead.StringGrid1.Height+85;
  FHead.Panel6.Visible:=True;
END;    {FormTabl_OmegaK}
{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}



end.



